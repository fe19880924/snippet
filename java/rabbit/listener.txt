@RabbitListener(
        bindings = @QueueBinding(
                value = @Queue(value = "{消费者queue}"),
                exchange = @Exchange(value = "{exchange}", type = ExchangeTypes.TOPIC),
                key = "{routingKey}"
        )
)