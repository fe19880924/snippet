package com.fclassroom.ssp.fss.manager;

import com.fclassroom.ssp.fss.dto.vo.CallbackPolicyResponse;
import com.fclassroom.ssp.fss.properties.AliyunOssProperties;

import com.aliyun.oss.HttpMethod;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.common.utils.BinaryUtil;
import com.aliyun.oss.internal.OSSUtils;
import com.aliyun.oss.model.Callback;
import com.aliyun.oss.model.DeleteObjectsRequest;
import com.aliyun.oss.model.GeneratePresignedUrlRequest;
import com.aliyun.oss.model.MatchMode;
import com.aliyun.oss.model.PolicyConditions;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * fss client
 */
@Service
public class FssClientHandler implements InitializingBean {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private AliyunOssProperties properties;
    private OSSClient ossClient;

    public List<String> deleteObjects(String bucketName, List<String> keyList) {
        DeleteObjectsRequest request = new DeleteObjectsRequest(bucketName);
        request.setKeys(keyList);
        return ossClient.deleteObjects(request).getDeletedObjects();
    }

    public void pubObject(String bucketName, String key, File file) {
        ossClient.putObject(bucketName, key, file);
    }

    public void putObject(String bucketName, String key, InputStream input) {
        ossClient.putObject(bucketName, key, input);
    }

    public String generatePresignedUrl(String bucketName, String key, int expireTime, boolean isPutMethod) {
        HttpMethod method;
        if (isPutMethod) {
            method = HttpMethod.PUT;
        } else {
            method = HttpMethod.POST;
        }
        GeneratePresignedUrlRequest request = new GeneratePresignedUrlRequest(bucketName, key, method);
        if (method.equals(HttpMethod.PUT)) {
            request.setContentType("application/octet-stream");
        }
        request.setExpiration(new Date(System.currentTimeMillis() + expireTime * 1000L));
        return ossClient.generatePresignedUrl(request).toString();
    }

    public CallbackPolicyResponse generateCallbackPolicy(
            String bucketName,
            String key,
            String callbackUrl,
            int expireTime,
            Map<String, Object> params) {
        long expireEndTime = System.currentTimeMillis() + expireTime * 1000L;
        Date expiration = new Date(expireEndTime);
        PolicyConditions policyConds = new PolicyConditions();
        policyConds.addConditionItem(PolicyConditions.COND_CONTENT_LENGTH_RANGE, 0, 1048576000);
        policyConds.addConditionItem(MatchMode.StartWith, PolicyConditions.COND_KEY, key);
        String postPolicy = ossClient.generatePostPolicy(expiration, policyConds);
        byte[] binaryData;
        try {
            binaryData = postPolicy.getBytes("utf-8");
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException("编码错误", e);
        }
        String encodedPolicy = BinaryUtil.toBase64String(binaryData);
        String postSignature = ossClient.calculatePostSignature(postPolicy);
        CallbackPolicyResponse response = new CallbackPolicyResponse();
        response.setAccessid(properties.getAccessKeyId());
        response.setPolicy(encodedPolicy);
        response.setSignature(postSignature);
        response.setDir(key);
        response.setHost("http://" + bucketName + "." + properties.getEndpoint());
        response.setExpire((int) (expireEndTime / 1000));
        Callback callback = new Callback();
        StringBuilder callbackBody = new StringBuilder();
        callbackBody.append("bucket=${bucket}&object=${object}&etag=${etag}&size=${size}&mimeType=${mimeType}");
        if (params != null) {
            for (Map.Entry<String, Object> entry : params.entrySet()) {
                callbackBody.append('&').append(entry.getKey()).append("=").append(entry.getValue());
            }
        }
        callback.setCallbackBody(callbackBody.toString());
        callback.setCallbackUrl(callbackUrl);
        callback.setCalbackBodyType(Callback.CalbackBodyType.JSON);
        response.setCallback(BinaryUtil.toBase64String(OSSUtils.jsonizeCallback(callback).getBytes()));
        return response;
    }

    /**
     * 验证callback
     */
    public boolean verifyCallback(String autorizationInput,
                                             String pubKeyInput,
                                             String callbackBody,
                                             String queryString,
                                             String uri) {
        byte[] pubKey = BinaryUtil.fromBase64String(pubKeyInput);
        String pubKeyAddr = new String(pubKey);
        if (!pubKeyAddr.startsWith("http://gosspublic.alicdn.com/")
                && !pubKeyAddr.startsWith("https://gosspublic.alicdn.com/")) {
            logger.warn("pub key addr must be oss addrss");
            return false;
        }
        String retString = executeGet(pubKeyAddr);
        if (retString == null) {
            return false;
        }
        retString = retString.replace("-----BEGIN PUBLIC KEY-----", "");
        retString = retString.replace("-----END PUBLIC KEY-----", "");
        String authStr = null;
        try {
            authStr = java.net.URLDecoder.decode(uri, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            logger.error("编码错误", e);
            return false;
        }
        if (queryString != null && !queryString.equals("")) {
            authStr += "?" + queryString;
        }
        authStr += "\n" + callbackBody;
        return doCheck(authStr, BinaryUtil.fromBase64String(autorizationInput), retString);
    }

    /**
     * 发送get请求
     *
     * @param url url
     * @return 请求结果
     */
    private String executeGet(String url) {
        BufferedReader in = null;
        try {
            HttpGet request = new HttpGet();
            request.setURI(new URI(url));
            HttpResponse response = HttpClientBuilder.create().build().execute(request);

            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuilder sb = new StringBuilder("");
            String line = null;
            String nl = System.getProperty("line.separator");
            while ((line = in.readLine()) != null) {
                sb.append(line).append(nl);
            }
            return sb.toString();
        } catch (IOException | URISyntaxException e) {
            return null;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e) {
                    logger.warn("close error", e);
                }
            }
        }
    }

    /**
     * 执行验证
     *
     * @param content   内容
     * @param sign      签名
     * @param publicKey 公钥
     * @return 验证结果
     */
    private boolean doCheck(String content, byte[] sign, String publicKey) {
        try {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            byte[] encodedKey = BinaryUtil.fromBase64String(publicKey);
            PublicKey pubKey = keyFactory.generatePublic(new X509EncodedKeySpec(encodedKey));
            java.security.Signature signature = java.security.Signature.getInstance("MD5withRSA");
            signature.initVerify(pubKey);
            signature.update(content.getBytes());
            return signature.verify(sign);
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        ossClient = new OSSClient(
                properties.getEndpoint(),
                properties.getAccessKeyId(),
                properties.getAccessKeySecret());
        logger.info("fss is start!");
    }
}
